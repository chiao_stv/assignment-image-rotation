//
// Created by pauline on 31.10.22.
//
#include "../include/bmp_rw.h"
#include "../include/image_rotate.h"

struct image rotate(const struct image *source){
    struct image rotate_image = create_image(source->height,source->width);
    for(uint32_t i = 0; i < source->height; i++){
        for(uint32_t j = 0; j < source->width; j++) {
            rotate_image.data[j * source->height+ (source->height - 1 - i)]= source->data[source->width * i + j];
        }
    }

    return rotate_image;
}
