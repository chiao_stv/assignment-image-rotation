#include "../include/bmp_rw.h"
#include "../include/image_rotate.h"
#include <stdio.h>





int main(int argc, char **argv) {

    if( argc != 3 ){
     fprintf(stderr, "Wrong amount of arguments");
    return 1;
    }


    const char *path_in = argv[1];
    const char *path_out = argv[2];
    //char *path_in ="/home/pauline/Загрузки/snail.bmp";
    //char *path_out ="/home/pauline/Загрузки/output.bmp";
    FILE *in;
    FILE *out;
    struct image input ={0};
    in = fopen(path_in,"rb");
    out = fopen(path_out,"wb");
    read_bmp(in,&input);
    struct image rotated_img = rotate(&input);
    write_bmp(out,&rotated_img);
    fclose(in);
    fclose(out);



    image_destroy(&input);
    image_destroy(&rotated_img);



    return 0;
}
