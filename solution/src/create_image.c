//
// Created by pauline on 02.11.22.
//
#include "../include/image.h"
#include <malloc.h>

struct image create_image(uint64_t width, uint64_t height){
    struct image image;
    image.width = width;
    image.height = height;
    image.data = malloc(width*height*sizeof (struct pixel));
    return image;
}
