//
// Created by pauline on 31.10.22.
//
#include "../include/bmp_header.h"
#include "../include/bmp_rw.h"
#include  <stdint.h>


static uint8_t number_padding(uint32_t width){
    uint8_t padding;
    if(width%4 ==0){
        padding =width%4;
    } else{
        padding = 4-(width*3)%4;
    }
    return padding;
}

static size_t size_of_image(const struct image* image){
    uint32_t size_image = image->height*image->width*sizeof(struct pixel)+ number_padding(image->width)*image->height;
    return size_image;
}
struct bmp_header read_header(FILE * in){
    struct bmp_header header;
    fread(&header,1 ,sizeof (struct bmp_header),in);
    return header;
}

static struct bmp_header create_header(struct image const *img) {
    struct bmp_header header;
    header.bfType = 0x4d42,
    header.biWidth = img->width,
    header.biHeight = img->height,
    header.bfileSize = 54+ size_of_image(img),
    header.bOffBits = 54,
    header.biSize = 40,
    header.biPlanes = 1,
    header.biBitCount = 24,
    header.biCompression = 0,
    header.biSizeImage = size_of_image(img),
    header.biXPelsPerMeter = 0,
    header.biYPelsPerMeter = 0,
    header.biClrImportant = 0,
    header.bfReserved = 0,
    header.biClrUsed = 0;
    return header;

}
enum read_status header_check(struct bmp_header header){
    if (header.biBitCount!=24){
        return READ_INVALID_BITS;
    }
    if (header.bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biCompression != 0) {
        return READ_INVALID_HEADER;
    }
    return READ_OK_HEADER;
}

enum read_status read_bmp(FILE* in, struct image* img){
    struct bmp_header header = read_header(in);
    header_check(header);
    *img = create_image(header.biWidth,header.biHeight);
    const uint8_t padding = number_padding(img->width);

    for (size_t i = 0; i <  header.biHeight ; i++){
        fread(&(img->data[i * img->width]), sizeof(struct pixel), img->width, in);
        for (size_t j =0;j<padding;j++){
            fseek(in,1,SEEK_CUR);
            if(ferror(in)){
                return READ_ERROR;
            }

        }
    }
    return READ_OK;

}

enum write_status write_bmp(FILE* out, struct image const* img){
    struct bmp_header header = create_header(img);
    const char zeroying[3] = {0};
    const uint32_t width = img->width;
    const uint32_t height = img ->height;
    uint8_t padding = number_padding(width);
    if(!fwrite(&header, sizeof(struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }


    for (uint32_t i = 0; i < height; ++i) {
        for (uint32_t j = 0; j <width ; ++j) {
            fwrite(&img->data[i*width+j],sizeof (struct pixel),1,out);
            if (ferror(out)){
                return WRITE_ERROR;
            }
        }
        fwrite(zeroying, 1, padding, out);
    }
    return WRITE_OK;
}
