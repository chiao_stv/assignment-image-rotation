//
// Created by pauline on 21.01.23.
//

#ifndef LAB_BMP_H
#define LAB_BMP_H
#include "../include/enums.h"
#include "../include/image.h"

#include <stdint.h>
#include <stdio.h>

enum read_status read_bmp(FILE* IN, struct image* img);

enum write_status write_bmp(FILE* OUT, const struct image* img);

#endif //LAB_BMP_H
