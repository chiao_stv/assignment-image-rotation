//
// Created by pauline on 31.10.22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATIO_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATIO_IMAGE_H
#include <stdint.h>
#include <stdio.h>

struct pixel{
    uint8_t r;
    uint8_t g;
    uint8_t  b;
};
struct image{
    uint64_t height,width;
    struct pixel* data;
};
struct image create_image(uint64_t width, uint64_t height);
void image_destroy(struct image *img);


#endif //ASSIGNMENT_IMAGE_ROTATIO_IMAGE_H


