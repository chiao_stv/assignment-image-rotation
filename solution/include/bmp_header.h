//
// Created by pauline on 21.01.23.
//

#ifndef LAB_BMP_HEADER_H
#define LAB_BMP_HEADER_H
#include "../include/enums.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
enum read_status header_check(struct bmp_header header);
struct bmp_header read_header(FILE *IN);
#endif //LAB_BMP_HEADER_H
