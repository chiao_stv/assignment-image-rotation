//
// Created by pauline on 21.01.23.
//

#ifndef LAB_ENUMS_H
#define LAB_ENUMS_H
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PADDING,
    READ_OK_HEADER,
    READ_ERROR
};
enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};
#endif //LAB_ENUMS_H
